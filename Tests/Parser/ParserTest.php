<?php

namespace Tests\Parser;

require_once 'vendor/autoload.php';
require_once 'Parser/Parser.php';

class ParserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * The parser instance
     * @var \Parser\Parser
     */
    private $parser;

    /**
     * Initialise the object
     */
    public function setUp()
    {
        $this->parser = new \Parser\Parser();
    }

    /**
     * Test for valid JSON
     * @return void
     */
    public function testValidJson()
    {
        $result = $this->parser->run();

        json_decode($result);
        $this->assertEquals(JSON_ERROR_NONE, json_last_error());
    }

    /**
     * Test the layout of the result objects
     * @return void
     */
    public function testLayout()
    {
        $result = $this->parser->run();

        $decoded = json_decode($result, true);

        // Check we have a "results" node
        $this->assertArrayHasKey('results', $decoded);

        // Check results is an array and there is at least one item
        $this->assertInternalType('array', $decoded['results']);
        $this->assertNotEmpty($decoded['results']);

        // Check the layout of every item
        foreach ($decoded['results'] as $item) {
            $this->assertArrayHasKey('title', $item);
            $this->assertArrayHasKey('size', $item);
            $this->assertArrayHasKey('unit_price', $item);
            $this->assertTrue(is_numeric($item['unit_price']));
            $this->assertArrayHasKey('description', $item);
        }
    }

    /**
     * Test whether the total exists and is accurate
     * @return void
     */
    public function testTotals()
    {
        $result = $this->parser->run();

        $decoded = json_decode($result, true);

        // Check we have a "total" node
        $this->assertArrayHasKey('total', $decoded);
        $this->assertTrue(is_numeric($decoded['total']));

        $productTotal = 0;
        foreach ($decoded['results'] as $product) {
            $productTotal += (float) $product['unit_price'];
        }

        $this->assertEquals($productTotal, $decoded['total']);
    }
}
