<?php

require_once 'vendor/autoload.php';
require_once 'Parser/Parser.php';

$parser = new Parser\Parser();

echo $parser->run();
