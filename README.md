# Readme

## Sainsbury's Parser Test

_Rob Lowcock - 4 October 2015_

### Installation
Composer packages must be installed beforehand, which can be done using `composer install`.

### Running
Once installed, the parser can be run from the root directory with `php parse.php`

### Unit tests
If dev dependencies are installed, unit tests can be run using `vendor/bin/phpunit Tests/Parser/ParserTest.php`

### Composer packages
The following Composer packages have been used:

* guzzlehttp/guzzle 5.3
* symfony/dom-crawler 2.7
* symfony/css-selector 2.7

PHP version 5.4 or higher is required.

### Notes
* The JSON format uses strings for prices, as a float would remove trailing zeros.
* Although Guzzle 6 and PHPUnit 5 have been released, both require versions of PHP higher than 5.4, and so older verisons of both packages have been used instead. This does compromise compatibility with PSR-7, which was introduced in Guzzle 6.