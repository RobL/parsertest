<?php

namespace Parser;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Parser\Parser
 *
 * Parse the Sainsbury's groceries page to generate a JSON layout
 *
 * @category Parser
 * @package Parser
 * @subpackage Parser
 * @author Rob Lowcock <rob.lowcock@gmail.com>
 */
class Parser
{
    /**
     * The target URL to parse
     * @var string
     */
    private $targetUrl = '';

    /**
     * Constructor
     * @param string $url The URL to try to parse
     */
    public function __construct($url = '')
    {
        $this->targetUrl = 'http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?'
                        . 'listView=true&orderBy=FAVOURITES_FIRST&parent_category_rn=12518'
                        . '&top_category=12518&langId=44&beginIndex=0&pageSize=20'
                        . '&catalogId=10137&searchTerm=&categoryId=185749&listId='
                        . '&storeId=10151&promotionId=#langId=44&storeId=10151'
                        . '&catalogId=10137&categoryId=185749&parent_category_rn=12518'
                        . '&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST'
                        . '&searchTerm=&beginIndex=0&hideFilters=true';

        if (!empty($url)) {
            $this->targetUrl = $url;
        }
    }

    /**
     * Run the parser
     * @return string A JSON-encoded array of the results
     */
    public function run()
    {
        $page = $this->getPage($this->targetUrl);

        return json_encode($this->getResults($this->getProducts($page)));
    }

    /**
     * Get the page
     * @param  string                       $url    The target URL
     * @return \GuzzleHttp\Message\Response          The Guzzle response object
     */
    private function getPage($url)
    {
        $client = new Client();

        $cookies = ['SESSION_COOKIEACCEPT' => 'true'];
        $jar = CookieJar::fromArray($cookies, 'sainsburys.co.uk');

        $response = $client->get($url, ['cookies' => $jar]);

        return $response;
    }

    /**
     * Get the products, with a crawler attached to each
     * @param  \GuzzleHttp\Message\Response $page The page
     * @return array                              The array of crawlers
     */
    private function getProducts(\GuzzleHttp\Message\Response $page)
    {
        // Initialise the crawler class
        $crawler = new Crawler();
        $crawler->addContent($page->getBody()->getContents());

        // Get the child nodes
        $children = $crawler->filter('ul.productLister')->children();

        // Convert each node into a Crawler instance
        $products = $children->each(function (Crawler $node) {
            return $node;
        });

        return $products;
    }

    /**
     * Turn the products into results
     * @param  array $products The products
     * @return array           The formatted array of products
     */
    private function getResults($products)
    {
        $results = [];
        $total = 0;

        foreach ($products as $product) {
            // Get the details needed for the price
            $price = $product->filter('p.pricePerUnit')->text();
            preg_match("/[.0-9]+/", $price, $matches);

            // Get the details needed for the size and description
            $link = $product->filter('h3 > a')->attr('href');
            $page = $this->getPage($link)->getBody();

            $total += $matches[0];

            $results[] = [
                'title' => trim($product->filter('h3 > a')->text()),
                'size' => round($this->getSize($page) / 1024, 2) . 'kb',
                'unit_price' => $matches[0],
                'description' => $this->getDescription($page)
            ];
        }

        $output = [
            'results' => $results,
            'total' => number_format($total, 2)
        ];

        return $output;
    }

    /**
     * Return the size of a page in bytes
     * @param  \GuzzleHttp\Stream\Stream  $page The page stream
     * @return integer                          The size of the page in bytes
     */
    private function getSize(\GuzzleHttp\Stream\Stream $page)
    {
        return $page->getSize();
    }

    /**
     * Get a description of a product from a product page
     * @param  \GuzzleHttp\Stream\Stream $page The page stream
     * @return string                          A description of the product
     */
    private function getDescription(\GuzzleHttp\Stream\Stream $page)
    {
        $crawler = new Crawler();
        $crawler->addContent($page->getContents());
        return trim(strip_tags($crawler->filter('div.productText p')->text()));
    }
}